import it.uniroma1.di.tmancini.utils.*;
import it.uniroma1.di.tmancini.teaching.ai.SATCodec.*;
import java.util.*;

public class SATtoSchursLemma {

	public static void main(String args[]) throws java.io.IOException, java.io.FileNotFoundException {
		
		SATModelDecoder md = new SATModelDecoder(args);
		md.run();
		
		ArrayList<ArrayList<Integer>> boxes = new ArrayList<ArrayList<Integer>>(); // content of the boxes
		boxes.add( new ArrayList<Integer>());
		boxes.add( new ArrayList<Integer>());
		boxes.add( new ArrayList<Integer>());				
	
		int maxVar = md.getMaxVar();
		for (int i=1; i <= maxVar; i++) {
			Boolean v_i = md.getModelValue(i);
			if (v_i == null || !v_i) continue; // false literal
			
			SATModelDecoder.Var var = md.decodeVariable(i);
			// The only family of variables is "In[ball, box]"
			int ball = var.getIndices().get(0);
			int box = var.getIndices().get(1);
			boxes.get(box-1).add(ball);
		}
		
		// Print-out the content of the boxes
		
		for (int box = 0; box < 3; box++) {
			System.out.println("Box " + (box+1) + ": " + boxes.get(box));
		}		
	}
		
} //:~