import it.uniroma1.di.tmancini.utils.*;
import it.uniroma1.di.tmancini.teaching.ai.SATCodec.*;
import java.util.*;

public class SchursLemmaToSAT {

	public static void main(String args[]) {
		
		// Defines the allowed/requested command-line options and flags
		CmdLineOptions clo = new CmdLineOptions("SchursLemmaToSAT", "2013-08-26", "Toni Mancini (http://tmancini.di.uniroma1.it)", 
			"A simple SAT encoder of the Schur's lemma problem: \n\nGiven integer n>0, we want to put n balls (labelled 1 to n) into 3 bins in such a way that no three balls x,y,z such that x+y=z are in the same bin.");
		
		clo.addOption("n", "The number of balls (a positive integer)", true);
		clo.addOption("o", "The name of the output DIMACS file");
		

		clo.addFlag("debug", "Debug mode (output is not in DIMACS format)");

		// Parses the command-line arguments
		clo.parse(args);
		
		
		// Gets the value of an option
		int n = Integer.parseInt(clo.getOptionValue("n"));
		
		// Creates the encoder
		SATEncoder enc = new SATEncoder("SchursLemma (n=" + n + ")", clo.getOptionValue("o"));
		
		if(clo.isFlagSet("debug")) {
			enc.enableDebugMode(); // Enables debug mode: output is not in DIMACS format
		}
		
		// Defines two integer ranges: 1..n and 1..3
		IntRange ballRange = new IntRange("ball", 1, n);
		IntRange boxRange = new IntRange("box", 1, 3);
		
		RangeProduct twoBoxesRange = new RangeProduct("twoBoxes", boxRange, 2);
		
		// Defines the family of variables In_{i,j} with i ranging over 1..n and j ranging over 1..3
		enc.defineFamilyOfVariables("In", ballRange, boxRange );	

	
		// Defines problem constraints:
		List<Integer> tuple = null;
		Iterator<List<Integer>> it = null;
		
		// 1. Each ball is in at least one box
		for(int ball : ballRange.values()) {
			enc.addComment("Ball " + ball + " is in at least one box:");
			// Adds clause:  In[ball, 1] or ... or In[ball, 3]
			for(int box : boxRange.values()) {
				enc.addToClause("In", ball, box);
			}
			enc.endClause();
			
			// 2. Each ball is in at most one box
			enc.addComment("Ball " + ball + " is in at most one box:");
			// For each pair of boxes box1 and box2 (box2 > box1) 
			// adds clause:  -In[ball, box1] or -In[ball, box2]
			it = twoBoxesRange.iterator(RangeProduct.FILTER.ALLDIFF_ORDERED);
			while (it.hasNext()) {
				tuple = it.next();
				// In[ball,box1] --> -In[ball,box2]
				enc.addNegToClause("In", ball, tuple.get(0));
				enc.addNegToClause("In", ball, tuple.get(1));
				enc.endClause();
			}
		}	
		
		// 3. No three balls x,y,z such that x+y=z are in the same box
		// For each three balls ball1, ...
		for(int ball1=ballRange.getMin(); 
				ball1 <= ballRange.getMax()/2; ball1++) {
			// ...ball2 (>ball1)
			for(int ball2=ball1+1; 
					ball2<=ballRange.getMax()-ball1; ball2++) {
				// , ... ball3 such that ball1+ball2 = ball3
				int ball3 = ball1+ball2;				
				if (ball3 > ballRange.getMax()) break;
				
				// and for each box
				for(int box : boxRange.values()) {
					enc.addComment("Balls " + ball1 + ", " + ball2 + ", " + ball3 + " are not all in box " + box + ":");
					// Adds clause:
					// -In(ball1,box) or -In(ball2,box) or -In(ball3, box)
					enc.addNegToClause("In", ball1, box);
					enc.addNegToClause("In", ball2, box);					
					enc.addNegToClause("In", ball3, box);					
					enc.endClause();
				}
			}
		}
		
		// Finalizes everything, frees-up temp memory and closes the output file.
		enc.end();
	}


} //:~